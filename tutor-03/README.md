## Tutor 03

* Add Custom SASS (Custom Design)

* Nice Header and Footer

* General Layout: Terms, Taxonomy

* Apply Two Column Responsive Layout for Most Layout

* Nice Tag Badge in Tags Page

![Hugo Materialize: Tutor 03][hugo-materialize-preview-03]

[hugo-materialize-preview-03]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-03/hugo-materialize-preview.png
