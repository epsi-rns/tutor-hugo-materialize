# Hugo Materialize Test Drive

An example of Hugo site using Materialize for personal learning purpose.

This is basically a Materialize version of Hugo Tutorial.

![Hugo Materialize: Preview][hugo-materialize-preview]

-- -- --

## Links

### Hugo Step By Step

This repository:

* [Hugo Step by Step Repository][tutorial-hugo]

### Materialize Step By Step

Additional guidance:

* [Materialize Step by Step Repository][tutorial-materialize]

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-materialize/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/

-- -- --

## Chapter Step by Step

### Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Hugo

* General Layout: Base, List, Single

* Partials: HTML Head, Header, Footer

* Additional Layout: Post

* Basic Content

![Hugo Materialize: Tutor 01][hugo-materialize-preview-01]

-- -- --

### Tutor 02

* Add Materialize CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Materialize CSS

![Hugo Materialize: Tutor 02][hugo-materialize-preview-02]

-- -- --

### Tutor 03

* Add Custom SASS (Custom Design)

* Nice Header and Footer

* General Layout: Terms, Taxonomy

* Apply Two Column Responsive Layout for Most Layout

* Nice Tag Badge in Tags Page

![Hugo Materialize: Tutor 03][hugo-materialize-preview-03]

-- -- --

### Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo

* Additional Layout: Archives

* Article Index: By Year, List Tree (By Year and Month)

* Custom Output: YAML, TXT, JSON

![Hugo Materialize: Tutor 04][hugo-materialize-preview-04]

-- -- --

### Tutor 05

* Article Index: Apply Multi Column Responsive List

* Post: Header, Footer, Navigation

> Optional Feature

* Blog Pagination: Adjacent, Indicator, Responsive

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

![Hugo Materialize: Tutor 05][hugo-materialize-preview-05]

-- -- --

### Tutor 06

> Finishing

* Layout: Service

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes

* Post: Syntax Highlight

* Post: Bulma Title: CSS Fix

* Meta: HTML, SEO, Opengraph, Twitter

* RSS: Filter to Show Only Post Kind

> Finishing

![Hugo Materialize: Tutor 06][hugo-materialize-preview-06]

-- -- --

What do you think ?

[hugo-materialize-preview]:   https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/hugo-materialize-preview.png
[hugo-materialize-preview-01]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-01/hugo-materialize-preview.png
[hugo-materialize-preview-02]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-02/hugo-materialize-preview.png
[hugo-materialize-preview-03]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-03/hugo-materialize-preview.png
[hugo-materialize-preview-04]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-04/hugo-materialize-preview.png
[hugo-materialize-preview-05]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-05/hugo-materialize-preview.png
[hugo-materialize-preview-06]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-06/hugo-materialize-preview.png
