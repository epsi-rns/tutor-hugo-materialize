## Tutor 06

> Finishing

* Layout: Service

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes

* Post: Syntax Highlight

* Post: Bulma Title: CSS Fix

* Meta: HTML, SEO, Opengraph, Twitter

* RSS: Filter to Show Only Post Kind

> Finishing

![Hugo Materialize: Tutor 06][hugo-materialize-preview-06]

[hugo-materialize-preview-06]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-06/hugo-materialize-preview.png
