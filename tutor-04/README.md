## Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo

* Additional Layout: Archives

* Article Index: By Year, List Tree (By Year and Month)

* Custom Output: YAML, TXT, JSON

![Hugo Materialize: Tutor 04][hugo-materialize-preview-04]

[hugo-materialize-preview-04]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-04/hugo-materialize-preview.png
[hugo-materialize-preview-05]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-05/hugo-materialize-preview.png
[hugo-materialize-preview-06]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-06/hugo-materialize-preview.png
