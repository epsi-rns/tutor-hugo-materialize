+++
type       = "post"
title      = "Deftones - Be Quiet and Drive"
date       = 2015-03-25T07:35:05+07:00
categories = ["lyric"]
tags       = ["nu metal", "90s"]
slug       = "deftones-be-quiet-and-drive"
author     = "epsi"
+++

This town don't feel mine
I'm fast to get away, far
<!--more-->

I dressed you in her clothes
Now drive me far away, away, away

Just far away and I don't care where
Just far away I don't care
