## Tutor 05

* Article Index: Apply Multi Column Responsive List

* Post: Header, Footer, Navigation

> Optional Feature

* Blog Pagination: Adjacent, Indicator, Responsive

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

![Hugo Materialize: Tutor 05][hugo-materialize-preview-05]

[hugo-materialize-preview-05]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-05/hugo-materialize-preview.png
