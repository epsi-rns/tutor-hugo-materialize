## Tutor 02

* Add Materialize CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Materialize CSS

![Hugo Materialize: Tutor 02][hugo-materialize-preview-02]

[hugo-materialize-preview-02]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-02/hugo-materialize-preview.png
