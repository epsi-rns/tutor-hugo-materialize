## Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Hugo

* General Layout: Base, List, Single

* Partials: HTML Head, Header, Footer

* Additional Layout: Post

* Basic Content

![Hugo Materialize: Tutor 01][hugo-materialize-preview-01]

[hugo-materialize-preview-01]:https://gitlab.com/epsi-rns/tutor-hugo-materialize/raw/master/tutor-01/hugo-materialize-preview.png
